#!/usr/bin/env bash
# -------------------
# curl -s https://s.philipk.dk/phil | bash
SQLCONF="/etc/phil/sql_root_pw"
SQLREADPW=$(cat "$SQLCONF")

trap ctrl_c INT

function ctrl_c() {
        /bin/bash /usr/local/bin/phil && exit 0
}

# Say hello and introduce y0'self
# --------------------
# Linux username and ServerName for Apache virtualhost
USER=$(\
  dialog --title "Delete vhost" \
         --inputbox "Enter prefered username:" 8 40 \
  3>&1 1>&2 2>&3 3>&- \
)

DOMAIN=$(\
  dialog --title "Delete vhost" \
         --inputbox "Enter prefered domain:" 8 40 \
  3>&1 1>&2 2>&3 3>&- \
)

dialog  --menu "Are you sure?" 16 35 10 \
                1 "No" \
                2 "Yes" \
                2>/tmp/delete-vhost
num=`cat /tmp/delete-vhost`
# OK is pressed
if [ "$num" == "1" ]
    then
        DELETE=NO
    elif [ "$num" == "2" ]
    then
        DELETE=YES

# Cancel is pressed
else
	exec /usr/local/bin/phil
fi
 
# remove the temp file
rm -f /tmp/delete-vhost

if [ $DELETE = "YES" ]; then
	su - ${USER} -c "pm2 stop '${DOMAIN}'"
	su - ${USER} -c "pm2 delete '${DOMAIN}'"
	ps -o pid= -u ${USER} | xargs sudo kill -9
	sed -i "/${DOMAIN}/d" /etc/phil/node_list.sh
	sed -i "/${DOMAIN}/d" /etc/phil/node_startup.sh
	userdel ${USER}
	rm -rf /var/www/${USER}
	rm -rf /var/mail/${USER}
	rm -rf /etc/httpd/conf.d/${DOMAIN}.conf
	rm -rf /var/log/httpd/error-${DOMAIN}.log
	rm -rf /var/log/httpd/access-${DOMAIN}.log
	rm -rf /var/log/httpd/${DOMAIN}/
	service ghost-${USER} stop
	forever-service ${USER} ghost-${DOMAIN}
	sed -i "/$DOMAIN/d" /etc/phil/backup_cron
	sed -i "/$DOMAIN/,+1d" /etc/phil/users
	sed -i "/${USER}/,+8d" /etc/phil/pass
	sed -i "/${DOMAIN}/d" /etc/phil/ssl
	mysql -u root -p${SQLREADPW} -e "DROP USER '${USER}_sql'@'localhost'"
	mysql -u root -p${SQLREADPW} -e "DROP DATABASE ${USER}_db"
	mysql -u root -p${SQLREADPW} -e "flush privileges"
fi

# Back to Phil Menu
exec /usr/local/bin/phil