#!/usr/bin/env bash
# -------------------
# curl -s https://s.philipk.dk/phil | bash
USER_PASS=$(openssl rand -hex 6)
THEDATE=`date +%H%M_%d%m%y`
STARTTIME=`date +%s`
SQLCONF="/etc/phil/sql_root_pw"
SQLREADPW=$(cat "$SQLCONF")
DB_USER=''
DB_PASS=''
DB_NAME=''

trap ctrl_c INT

function ctrl_c() {
        /bin/bash /usr/local/bin/phil && exit 0
}

ar=()
while read n s ; do
    ar+=($n "$s")
done < /etc/phil/users 
dialog  --menu "_________________ Domain _____________________ _________________ User _______________________" 20 50 30 "${ar[@]}"

USER=$(\
  dialog --title "Clone vhost" \
         --inputbox "Enter prefered username (original):" 8 40 \
  3>&1 1>&2 2>&3 3>&- \
)

WWW_DIR=/var/www/${USER}

if [ -f ${WWW_DIR}/app/etc/local.xml ]; then
	DB_USER=`cat ${WWW_DIR}/app/etc/local.xml | grep username | sed 's/[ ]*<username><!\[CDATA\[\(.*\)\]\]><\/username>/\1/'`
	DB_PASS=`cat ${WWW_DIR}/app/etc/local.xml | grep password | sed 's/[ ]*<password><!\[CDATA\[\(.*\)\]\]><\/password>/\1/'`
	DB_NAME=`cat ${WWW_DIR}/app/etc/local.xml | grep dbname | sed 's/[ ]*<dbname><!\[CDATA\[\(.*\)\]\]><\/dbname>/\1/'`
fi

if [ -f ${WWW_DIR}/wp-config.php ]; then
	DB_NAME=`cat ${WWW_DIR}/wp-config.php | grep DB_NAME | cut -d \' -f 4`
	DB_USER=`cat ${WWW_DIR}/wp-config.php | grep DB_USER | cut -d \' -f 4`
	DB_PASS=`cat ${WWW_DIR}/wp-config.php | grep DB_PASSWORD | cut -d \' -f 4`
fi

rm -rf /tmp/clone-info
echo "+ CONFIG :	$WWW_DIR/wp-config.php" >> /tmp/clone-info
echo "_ DB_USER:	$DB_USER" >> /tmp/clone-info
echo "_ DB_PASS:	$DB_PASS" >> /tmp/clone-info
echo "_ DB_NAME:	$DB_NAME" >> /tmp/clone-info

ar=()
while read n s ; do
    ar+=($n "$s")
done < /tmp/clone-info 
dialog  --menu "_________________ Info _____________________ " 20 50 30 "${ar[@]}"

ORIGINAL_SITE=$(\
  dialog --title "Clone vhost" \
         --inputbox "Enter source url (original):" 8 40 \
  3>&1 1>&2 2>&3 3>&- \
  )

USER_CLONE=$(\
  dialog --title "Clone vhost" \
         --inputbox "Enter prefered username (clone):" 8 40 \
  3>&1 1>&2 2>&3 3>&- \
)


CLONE_SITE=$(\
  dialog --title "Clone vhost" \
         --inputbox "Enter target url (clone):" 8 40 \
  3>&1 1>&2 2>&3 3>&- \
  )

VHOST=$(cat <<EOF
<VirtualHost *:80>
  DocumentRoot "/var/www/${USER_CLONE}"
  ServerName ${CLONE_SITE}
  ServerAlias www.${CLONE_SITE}
  <Directory "/var/www/${USER_CLONE}">
    AllowOverride All
    Require all granted
  </Directory>
  <ifmodule mpm_itk_module>
    AssignUserID ${USER_CLONE} apache
  </ifmodule>
  ErrorLog /var/log/httpd/${CLONE_SITE}/error.log
  CustomLog /var/log/httpd/${CLONE_SITE}/access.log combined
</VirtualHost>
EOF
)

echo -e "\e[38;5;75m>>> \e[38;5;15m checking for user - otherwise create"
if grep -q ${USER_CLONE} /etc/passwd;
    then 
        echo -e "\e[38;5;75m>>> \e[38;5;15m local user already existing on staging server"
        echo -e "${USER_CLONE}:${USER_PASS}" | chpasswd
    else
	    useradd ${USER_CLONE} -d /var/www/${USER_CLONE} -G apache -s /sbin/nologin
	    echo -e "${USER_CLONE}:${USER_PASS}" | chpasswd 
	    chgrp -R apache /var/www/${USER_CLONE}
	    chmod -R g+w /var/www/${USER_CLONE}
	    chmod g+s /var/www/${USER_CLONE}
	    chmod 775 /var/www/${USER_CLONE}
	    find /var/www/${USER_CLONE} -type f -exec chmod 664 {} \;
	    find /var/www/${USER_CLONE} -type d -exec chmod 775 {} \;
fi

echo "$VHOST" > /etc/httpd/conf.d/${CLONE_SITE}.conf
mkdir -p /var/log/httpd/${CLONE_SITE}

systemctl restart httpd.service

DB_CUSER=${USER_CLONE}_sql 
DB_CNAME=${USER_CLONE}_db
mysql -u root -p${SQLREADPW} -e "DROP DATABASE ${DB_CNAME}"
mysql -u root -p${SQLREADPW} -e "CREATE DATABASE ${DB_CNAME}"
mysql -u root -p${SQLREADPW} -e "GRANT USAGE ON *.* TO ${DB_CUSER}@localhost IDENTIFIED BY '${DB_PASS}'"
mysql -u root -p${SQLREADPW} -e "GRANT ALL PRIVILEGES ON ${DB_CNAME}.* TO ${DB_CUSER}@localhost"
mysql -u root -p${SQLREADPW} -e "FLUSH PRIVILEGES"

echo -e "\e[38;5;75m>>> \e[38;5;15m dumping database to /tmp and setting clone URL in database"
mysqldump -u ${DB_USER} -p${DB_PASS} ${DB_NAME} > /tmp/db_${USER}_${THEDATE}.sql
sed -i "s/$ORIGINAL_SITE/$CLONE_SITE/g" /tmp/db_${USER}_${THEDATE}.sql

echo -e "\e[38;5;75m>>> \e[38;5;15m transfering database and www files to local clone"
rsync -azhL --exclude='var/cache/*' --exclude='var/session/*' --exclude='var/session/report/*' ${WWW_DIR}/ /var/www/${USER_CLONE}
chown ${USER_CLONE}:apache /var/www/${USER_CLONE} -Rf
if [ -f ${WWW_DIR}/app/etc/local.xml ]; then
	sed -i "s/${DB_USER}/${DB_USER}_c/g" /var/www/${USER_CLONE}/app/etc/local.xml
	sed -i "s/${DB_NAME}/${DB_NAME}_c/g" /var/www/${USER_CLONE}/app/etc/local.xml
	find /var/www/${USER_CLONE}/ -type d -exec chmod 0755 {} \; 
	find /var/www/${USER_CLONE}/ -type f -exec chmod 0644 {} \;
	chmod -R 775 /var/www/${USER_CLONE}/media /var/www/${USER_CLONE}/var
	chmod 775 /var/www/${USER_CLONE}/app/etc
fi

if [ -f ${WWW_DIR}/wp-config.php ]; then
	sed -i "s/${DB_USER}/${DB_CUSER}/g" /var/www/${USER_CLONE}/wp-config.php
	sed -i "s/${DB_NAME}/${DB_CNAME}/g" /var/www/${USER_CLONE}/wp-config.php
	find /var/www/${USER_CLONE}/ -type d -exec chmod 0775 {} \; 
	find /var/www/${USER_CLONE}/ -type f -exec chmod 0664 {} \;
fi

echo -e "\e[38;5;75m>>> \e[38;5;15m loading database and www files on local clone"
cd /tmp
mysql -u ${DB_CUSER} -p${DB_PASS} ${DB_CNAME} < /tmp/db_${USER}_${THEDATE}.sql
rm -rf /tmp/db_${USER}_${THEDATE}.sql

echo -e "\e[38;5;75m>>> \e[38;5;15m time spent on moving to local clone:"
ENDTIME=`date +%s`
RUNTIME=$((ENDTIME-STARTTIME))
echo -e "\e[38;5;75m>>> \e[38;5;15m ${RUNTIME} seconds"
echo -e " "
echo -e "\e[38;5;75m>>> \e[38;5;15m username:   	${USER}_c"
echo -e "\e[38;5;75m>>> \e[38;5;15m password:   	${USER_PASS}"
echo -e "\e[38;5;75m>>> \e[38;5;15m Secure shell:    	ssh ${USER}_c@${CLONE_SITE} -p 2200"
echo -e " "

# Put this nice and "safely" to a very top secret file...
# -------------------
#
echo -e "# ${USER_CLONE} - SSH/SFTP #######################" >> /etc/phil/pass
echo -e "username:   ${USER_CLONE}" >> /etc/phil/pass
echo -e "password:   ${USER_PASS}" >> /etc/phil/pass
echo -e "# ${USER_CLONE} - MySQL ##########################" >> /etc/phil/pass
echo -e "username:   ${USER}_sql_c" >> /etc/phil/pass
echo -e "password:   ${DB_PASS}" >> /etc/phil/pass
echo -e "database:   ${USER}_db_c" >> /etc/phil/pass
echo -e " " >> /etc/phil/pass
#echo -e "${USER} <${DOMAIN}> - created ${TIMESTAMP}" >> /etc/phil/users
echo -e "+	${CLONE_SITE}" >> /etc/phil/users
echo -e "_	${USER_CLONE}" >> /etc/phil/users

# Back to Phil Menu
exec /usr/local/bin/phil
